class WaterFallFlow extends HTMLElement {
  constructor() {
    super();

    const shadowDom = this.attachShadow({ mode: "open" });
    const styleEle = document.createElement("style");
    styleEle.textContent = `
    * {
      margin: 0px;
      padding: 0px;
      list-style: none;
      font-style: normal;
      font-family: Microsoft YaHei, arial;
    }
    .twoColumnBox {
      margin: 6px 8px;
      overflow: hidden;
      padding-bottom: 25px;
    }
  
    .twoColumnBox ul {
      float: left;
      width: 49%;
    }
  
    .twoColumnBox ul:last-child {
      margin-left: 2%;
    }
  
    .twoColumnBox ul li {
      margin-bottom: 6px;
      padding: 5px;
      background-color: #ffffff;
    }
  
    .twoColumnBox ul li p:first-child {
      padding-top: 0px;
    }
  
    .twoColumnBox ul li p {
      padding-top: 4px;
    }
  
    .picBox img {
      display: block;
      width: 100%;
    }
  
    .namePrice {
      overflow: hidden;
      line-height: 20px;
    }
  
    .namePrice a {
      display: block;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
  
    .namePrice a:first-child {
      font-size: 0.9em;
      color: #58b7e3;
      width: 65%;
      float: left;
    }
  
    .namePrice a:last-child {
      font-size: 0.8em;
      color: #f00;
      float: right;
      width: 35%;
      text-align: right;
    }
  
    .detailBox {
      font-size: 0.8em;
      color: #777;
    }`;
    const json = {
      data: [
        {
          name: "石头石头石头",
          price: "128",
          details: "画圈画圈画圈",
          src: "img/i1.png"
        },
        {
          name: "植物植物植物",
          price: "298",
          details: "天然天然天然",
          src: "img/i2.png"
        },
        {
          name: "珍珠珍珠珍珠",
          price: "668",
          details: "珍珠珍珠",
          src: "img/i3.png"
        },
        {
          name: "绿色石头",
          price: "218",
          details: "石头绿色石头",
          src: "img/i4.png"
        },
        {
          name: "小葫芦吊坠",
          price: "88",
          details: "小葫芦吊坠小葫芦吊坠小葫芦吊",
          src: "img/i5.png"
        },
        {
          name: "天然灵芝",
          price: "128",
          details: "灵芝灵芝灵芝",
          src: "img/i6.png"
        },
        {
          name: "无规则石头",
          price: "128",
          details: "无规则石头无规则石头",
          src: "img/i7.png"
        },
        {
          name: "植物植物植物",
          price: "298",
          details: "质量超好特别的舒适，夏天首选。",
          src: "img/i8.png"
        },
        {
          name: "光滑的石头",
          price: "668",
          details: "光滑的石头光滑的石头光滑的石头",
          src: "img/i9.png"
        },
        {
          name: "春兰灿宝",
          price: "218",
          details: "三支花三支花",
          src: "img/i10.png"
        },
        {
          name: "春兰灿宝三支花",
          price: "118",
          details: "三支花三支花",
          src: "img/i11.png"
        }
      ]
    };
    const divRootEle = document.createElement("div");
    divRootEle.classList.add("twoColumnBox");

    const ulLeftEle = document.createElement("ul");
    ulLeftEle.classList.add("boxLeft");

    const ulRightEle = document.createElement("ul");
    ulRightEle.classList.add("boxRight");

    const tpl = document.getElementById("J_tpl_li");
    const tplContentLi = tpl.content.querySelector("li");
    shadowDom.appendChild(styleEle);

    // 创建两个div，把数据倒入高度小的div。
    for (var i = 0; i < json.data.length; i++) {
      const imgEle = tplContentLi.querySelector(".picBox img");
      imgEle.src = json.data[i].src;

      const nameEle = tplContentLi.querySelectorAll(".namePrice a");
      nameEle[0].textContent = json.data[i].name;
      nameEle[1].textContent = json.data[i].price;

      const detailEle = tplContentLi.querySelector(".detailBox");
      detailEle.textContent = json.data[i].details;

      const cloneNodes = tpl.content.cloneNode(true);

      if (ulLeftEle.offsetHeight < ulRightEle.offsetHeight) {
        ulLeftEle.appendChild(cloneNodes);
        divRootEle.appendChild(ulLeftEle);
      } else {
        ulRightEle.appendChild(cloneNodes);
        divRootEle.appendChild(ulRightEle);
      }
      shadowDom.appendChild(divRootEle);
    }
  }
}

customElements.define("water-fall-flow", WaterFallFlow);
