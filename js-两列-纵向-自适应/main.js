//模拟JSON数据
const json = {
    data: [
      {
        name: "石头石头石头",
        price: "128",
        details: "画圈画圈画圈",
        src: "img/i1.png"
      },
      {
        name: "植物植物植物",
        price: "298",
        details: "天然天然天然",
        src: "img/i2.png"
      },
      {
        name: "珍珠珍珠珍珠",
        price: "668",
        details: "珍珠珍珠",
        src: "img/i3.png"
      },
      {
        name: "绿色石头",
        price: "218",
        details: "石头绿色石头",
        src: "img/i4.png"
      },
      {
        name: "小葫芦吊坠",
        price: "88",
        details: "小葫芦吊坠小葫芦吊坠小葫芦吊",
        src: "img/i5.png"
      },
      {
        name: "天然灵芝",
        price: "128",
        details: "灵芝灵芝灵芝",
        src: "img/i6.png"
      },
      {
        name: "无规则石头",
        price: "128",
        details: "无规则石头无规则石头",
        src: "img/i7.png"
      },
      {
        name: "植物植物植物",
        price: "298",
        details: "质量超好特别的舒适，夏天首选。",
        src: "img/i8.png"
      },
      {
        name: "光滑的石头",
        price: "668",
        details: "光滑的石头光滑的石头光滑的石头",
        src: "img/i9.png"
      },
      {
        name: "春兰灿宝",
        price: "218",
        details: "三支花三支花",
        src: "img/i10.png"
      },
      {
        name: "春兰灿宝三支花",
        price: "118",
        details: "三支花三支花",
        src: "img/i11.png"
      }
    ]
  };

  const leftBox = document.querySelector(".boxLeft");
  const rightBox = document.querySelector(".boxRight");
  const tpl = document.getElementById("J_tpl_li");
  const tplContentLi = tpl.content.querySelector("li");

  // 创建两个div，把数据倒入高度小的div。
  for (var i = 0; i < json.data.length; i++) {
    const imgEle = tplContentLi.querySelector(".picBox img");
    imgEle.src = json.data[i].src;

    const nameEle = tplContentLi.querySelectorAll(".namePrice a");
    nameEle[0].textContent = json.data[i].name;
    nameEle[1].textContent = json.data[i].price;

    const detailEle = tplContentLi.querySelector(".detailBox");
    detailEle.textContent = json.data[i].details;

    const cloneNode = document.importNode(tpl.content, true);

    if (leftBox.offsetHeight < rightBox.offsetHeight) {
      leftBox.appendChild(cloneNode);
    } else {
      rightBox.appendChild(cloneNode);
    }
  }